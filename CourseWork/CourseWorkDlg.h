
// CourseWorkDlg.h : ���� ���������
//

#pragma once
#include "afxcmn.h"
#include "afxwin.h"


// ���������� ���� CCourseWorkDlg
class CCourseWorkDlg : public CDialogEx
{
// ��������
public:
	CCourseWorkDlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_COURSEWORK_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CSliderCtrl m_cProgress;
	CStatic m_Field;
	afx_msg void OnNMCustomdrawSlider1(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnStnClickedField();
	UINT m_ClusterNum;
	UINT m_PointNum;
	afx_msg void OnBnClickedGener();
	CString m_algor;
	CComboBox m_cAlgor;
	afx_msg void OnCbnSelendokMenu();
	int cTimer;
	CStatic cTimeCtrl;
};
