//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется CourseWork.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_COURSEWORK_DIALOG           102
#define IDR_MAINFRAME                   128
#define IDC_SLIDER1                     1007
#define IDC_FIELD                       1008
#define IDC_MENU                        1010
#define IDC_CLUSTER_COUNTER             1012
#define IDC_POINTS_COUNTER              1013
#define IDC_GENER                       1014
#define IDC_TIME                        1016

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1017
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
