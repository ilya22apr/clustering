#include "stdafx.h"
#include "CourseWork.h"
#include "CourseWorkDlg.h"
#include "afxdialogex.h"
#include "algorithm.h"
#include "knn.h"
#include "k-means.h"
//#include <ctime>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// ���������� ���� CAboutDlg ������������ ��� �������� �������� � ����������

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// ������ ����������� ����
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV

// ����������
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// ���������� ���� CCourseWorkDlg



CCourseWorkDlg::CCourseWorkDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCourseWorkDlg::IDD, pParent)
	, m_ClusterNum(3)
	, m_PointNum(50)
	, m_algor(_T(""))
	, cTimer(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCourseWorkDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SLIDER1, m_cProgress);
	DDX_Control(pDX, IDC_FIELD, m_Field);
	DDX_Text(pDX, IDC_CLUSTER_COUNTER, m_ClusterNum);
	DDX_Text(pDX, IDC_POINTS_COUNTER, m_PointNum);
	DDV_MinMaxUInt(pDX, m_PointNum, 1, 5000);
	DDV_MinMaxUInt(pDX, m_ClusterNum, 1, 1000);
	DDX_CBString(pDX, IDC_MENU, m_algor);
	DDX_Control(pDX, IDC_MENU, m_cAlgor);
	DDX_Text(pDX, IDC_TIME, cTimer);
	DDX_Control(pDX, IDC_TIME, cTimeCtrl);
}

BEGIN_MESSAGE_MAP(CCourseWorkDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER1, &CCourseWorkDlg::OnNMCustomdrawSlider1)
ON_STN_CLICKED(IDC_FIELD, &CCourseWorkDlg::OnStnClickedField)
ON_BN_CLICKED(IDC_GENER, &CCourseWorkDlg::OnBnClickedGener)
ON_CBN_SELENDOK(IDC_MENU, &CCourseWorkDlg::OnCbnSelendokMenu)
END_MESSAGE_MAP()


// ����������� ��������� CCourseWorkDlg

BOOL CCourseWorkDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ���������� ������ "� ���������..." � ��������� ����.

	// IDM_ABOUTBOX ������ ���� � �������� ��������� �������.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// ������ ������ ��� ����� ����������� ����. ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������

	// TODO: �������� �������������� �������������

	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

void CCourseWorkDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}


void CCourseWorkDlg::OnPaint()
{
	//CPaintDC dc(this); // �������� ���������� ��� ���������
	//dc.Ellipse(5,5,10,10);
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR CCourseWorkDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

class GraphData
{
public:
        static const GraphData& Instance()
        {
                static GraphData theSingleInstance;
                return theSingleInstance;
        }
private:        
        GraphData(){}
        GraphData(const GraphData& root);
        GraphData& operator=(const GraphData&);
};

struct Graph_Data
{
public:
	vector<cluster>* cl_p;
	Algorythm* algor;
	CBrush* pal;
	int Height; 
	int Width;
	int rad;
	CBrush* MakePalette(int k);
	void InitDrawing(CStatic& m_Field, CClientDC& dc);
	void DrawClusters(CClientDC& dc, vector<cluster>& clust);
} g_data;

CBrush* Graph_Data::MakePalette(int k)    //���������� ������ ��������� ������
{
	CBrush* pal = new CBrush[k];
	srand((unsigned int)time(NULL));
	for(int i = 0; i<k; i++)
	{
		pal[i].CreateSolidBrush(RGB(rand()%256, rand()%256, rand()%256));
	};
	return pal;
};

void Graph_Data::InitDrawing(CStatic& m_Field, CClientDC& dc)//������ ������� ���� � ���������� ��� �������
{
	CRect myfield;
	m_Field.GetWindowRect(myfield);
	CBrush brushWhite(RGB(255, 255, 255));
	dc.SelectObject(&brushWhite);
    dc.Rectangle(0,0,myfield.Width(),myfield.Height());
	Height = myfield.Height() - 2*12; 
	Width = myfield.Width() - 2*12;
};

void Graph_Data::DrawClusters(CClientDC& dc, vector<cluster>& clust)
{
	for(UINT i = 0; i<clust.size(); i++)
	{
		dc.SelectObject(pal[i]);
		int size = clust[i].get_size();
		for(int j=0; j<size; j++)
		{
			int x = clust[i].get_point(j).x + 12;
			int y = clust[i].get_point(j).y + 12;
			dc.Ellipse(x-rad,y-rad,x+rad,y+rad);
		};
	};
};

void CCourseWorkDlg::OnBnClickedGener()
{
	UpdateData(TRUE);
	CClientDC dc(&m_Field); // �������� ���������� ��� ���������
	g_data.InitDrawing(m_Field, dc);
	g_data.algor = new Algorythm(m_PointNum);
	g_data.algor->RandomizePoints(g_data.Width, g_data.Height);
	g_data.pal = g_data.MakePalette(m_ClusterNum);
	switch(m_cAlgor.GetCurSel())
	{
	case 0:
		{
			int begin = clock();
			g_data.cl_p = &g_data.algor->knn(m_ClusterNum);
			int end = clock();
			g_data.DrawClusters(dc, *g_data.cl_p);
			cTimer = end - begin;
			break;
		}
	case 1:
		{
			int begin = clock();
			g_data.cl_p = &g_data.algor->k_means(m_ClusterNum);
			int end = clock();
			g_data.DrawClusters(dc, *g_data.cl_p);
			cTimer = end - begin;
			break;
		}
	};
	UpdateData(FALSE);
}

void CCourseWorkDlg::OnNMCustomdrawSlider1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	m_cProgress.SetRange(5, 12, 0);
	UpdateData(TRUE);
	g_data.rad = m_cProgress.GetPos();

	if(g_data.cl_p != NULL){
	CClientDC dc(&m_Field);
	g_data.InitDrawing(m_Field, dc);
	switch(m_cAlgor.GetCurSel())
		{
	case 0:
		{
			g_data.DrawClusters(dc, *g_data.cl_p);
			break;
		}
	case 1:
		{
			g_data.DrawClusters(dc, *g_data.cl_p);
			break;
		}
	};}
	UpdateData(FALSE);
	*pResult = 0;
}


void CCourseWorkDlg::OnStnClickedField()
{
	// TODO: �������� ���� ��� ����������� �����������
}

void CCourseWorkDlg::OnCbnSelendokMenu()
{
	if(g_data.algor != NULL){
	UpdateData(TRUE);
	CClientDC dc(&m_Field);
	g_data.InitDrawing(m_Field, dc);
	switch(m_cAlgor.GetCurSel())
		{
	case 0:
		{
			int begin = clock();
			g_data.cl_p = &g_data.algor->knn(m_ClusterNum);
			int end = clock();
			g_data.DrawClusters(dc, *g_data.cl_p);
			cTimer = end - begin;
			break;
		}
	case 1:
		{
			int begin = clock();
			g_data.cl_p = &g_data.algor->k_means(m_ClusterNum);
			int end = clock();
			g_data.DrawClusters(dc, *g_data.cl_p);
			cTimer = end - begin;
			break;
		}
		};
	UpdateData(FALSE);
	}
}
