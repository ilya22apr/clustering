//#include "algorithm.h"

vector<cluster>& Algorythm::k_means(int k)
{
	int n = points.size();
	vector<cluster> &clust = *new vector<cluster>(n);
	srand((unsigned int)time(NULL));
	for(int i = 0; i<k; i++)       //������������ ��������� �� ��������� �����
	{
		int j = rand() % n;
		clust[i].set_cent(points[j]);
	};
	bool flag;
	do
	{
		for(int i = 0; i<k; i++){clust[i].clear_points();};//������� �������� �� ������ �����, ��� ��-������ ���������
		//������������ ������ ����� �� ��������� � ������� �������:
		for(int i = 0; i<n; i++)
		{
			int nearest = 0;// ������ ���������� ���������(�� �����. ������ �������)
			double min_metr = metric(points[i], clust[0].get_cent());//����� ����� ����� ����� ������ � ���������� 1-�� ��������
			//���� �������:
			for(int j = 1; j<k; j++)
			{
				double curr_metr = metric(points[i], clust[j].get_cent());
				if(curr_metr<min_metr)
					{
						min_metr = curr_metr;
						nearest = j;
					};
			};
			clust[nearest].add_point(points[i]);//��������� ���� ����� � ������� � ��������� ����������
		};
		flag = 0;  //���� ���������� ���� ���� �������� ����� 1
		for(int i = 0; i<k; i++)//������������� ���������
		{
			point temp = clust[i].get_cent();
			clust[i].calculate_centroid();
			if(temp != clust[i].get_cent()){flag = 1;};
		};
	} while(flag != 0);
	return clust;
};