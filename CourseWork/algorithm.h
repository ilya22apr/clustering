#include "stdafx.h"
#include "stdlib.h"
#include <cmath>
#include "time.h"
#include <vector>

using namespace std;

struct point
{
	int x, y;
};

bool operator !=(point & a, point & b)
{
	if(a.x == b.x && a.y == b.y)return false;
	else return true;
};

double metric(point &p1, point &p2)
{
	double x1 = p1.x, x2 = p2.x, y1 = p1.y, y2 = p2.y;
	double m = sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
	return m;
};

class cluster
{
	point centroid;
	vector<point> points;
public:
	void set_cent(point&point){this->centroid = point;};
	point& get_cent(){return centroid;};
	int get_size(){return points.size();};
	void calculate_centroid();           //вычисляет центроид как ср арифм координат точек в кластере
	void add_point(point&point){this->points.push_back(point);};
	void clear_points(){this->points.clear();};
	point &get_point(int i){return points[i];};
};

void cluster::calculate_centroid()
	{
		double x = 0.0, y = 0.0;
		int size = this->get_size();
		for(int i = 0; i<size; i++)
		{
			x += points[i].x;
			y += points[i].y;
		}
		this->centroid.x = floor(x/size);
		this->centroid.y = floor(y/size);
	};

class Algorythm
{
	int n;
	vector<point> points;
public:
	Algorythm(UINT cl_num);
	vector<cluster>& knn(int k);
	vector<cluster>& k_means(int k);
	void RandomizePoints(int Width, int Height);
};

Algorythm::Algorythm(UINT cl_num)
	{
		n = cl_num;
		points = *new vector<point>(n);
	};

void Algorythm::RandomizePoints(int Width, int Height)
{
	srand((unsigned int)time(NULL));
	for(int i=0; i<n; i++)
	{
		points[i].x = rand() % Width;
		points[i].y = rand() % Height;
	}
};