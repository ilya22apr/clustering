//#include "algorithm.h"

struct matrix    //������� ����������
{
	int size;
	double** arr;
	matrix(int n);
	~matrix();
	double** get_matr(){return this->arr;};
	void count_metr(vector<cluster> &clust);   //�������� ������� ����������
	void merge_min(vector<cluster> &clust);    //������� ��� �������� � ���� �����������
};

matrix::matrix(int n)
{
	size = n;
	arr = new double* [size];
	for(int i = 0; i < size; i++)
		arr[i] = new double[size];
	for(int i = 0; i < size; i++)
		for(int j = 0; j < size; j++)
			arr[i][j] = 0;
};

matrix::~matrix()
{
	for(int i = 0; i < size; i++) {delete [] arr[i];}
	delete [] arr;
};

void matrix::count_metr(vector<cluster> &clust)
{
	for(int i = 0; i < size; i++)
		for(int j = 0; j < size; j++)
			arr[i][j] = metric(clust[i].get_cent(), clust[j].get_cent());
};

void matrix::merge_min(vector<cluster> &clust)    //������� ��� �������� � ���� �����������
{
	int clust1 = 0, clust2 = 1; //������� ���� ���������
	double temp = arr[0][1];
	for(int i = 0; i < size; i++)    //���� �������� � ���� �����
		for(int j = 0; j < size; j++)
		{
			double curr = arr[i][j];
			if(curr < temp && curr != 0)
			{
				temp = curr;
				clust1 = i;
				clust2 = j;
			};
		};
	for(int i = 0; i < clust[clust2].get_size(); i++)          //�������� ��� ����� �� 2-�� � 1-�� �������
		clust[clust1].add_point(clust[clust2].get_point(i));
	clust[clust1].calculate_centroid();                 //�� �������� ����������� �������� ��������
	clust.erase(clust.begin()+clust2);                   //���������� ���������� �������!!!
	size--;
};

vector<cluster>& Algorythm::knn(int k)
{
	vector<cluster> &clust = *new vector<cluster>(points.size());
	for(unsigned int i = 0; i < points.size(); i++)   //������� ������ ����� �� ��������
	{
		clust[i].add_point(points[i]);
		clust[i].set_cent(points[i]);
	};
	while(clust.size() != k)
	{
		matrix matr(clust.size());
		matr.count_metr(clust);
		matr.merge_min(clust);
	};
	return clust;
};